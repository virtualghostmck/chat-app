# 💻 [Chat-Console](https://chat-box-clone.firebaseapp.com/)

##### ✒️ Welcome to my Chat-Console App. This is a *Slack-like* chat application developed using `ReactJS`, `Material UI` & `Firebase`.

##### ✒️ The desktop friendly application allows users to create `channels`, have `discussions` and send text `messages` as well as images to each other.

##### ✒️ The application has full `emoji` support and `image` files upload feature.

##### ✒️ Users could also update their *profile photo* and view the *top posters* in the channels.

##### ✒️ `Presence Service` is also integrated to identify and display the online status 🟢 of users.

<br/>

## ⚡ Demo

### 🎬 Login Screen

<img src="https://gitlab.com/virtualghostmck/chat-app/-/raw/master/login.gif" alt="login gif"> 

### 🎬 Chat Screen

<img src="https://gitlab.com/virtualghostmck/chat-app/-/raw/master/chat.gif" alt="chat gif">  


> **Login now and Feel free to play around with the app at [Chat-Console](https://chat-box-clone.firebaseapp.com/)** ⭐

> <img src="https://miro.medium.com/max/480/1*7LOWVelUHYS1iqeX34Whzg.png" width="100" > <img src="https://firebase.google.com/downloads/brand-guidelines/PNG/logo-built_white.png" width="100" > 



##### Reach out to me at : &nbsp;&nbsp; <a href="https://www.linkedin.com/in/mayuresh-kakade-ba4658125/"><img src="https://logodix.com/logo/91004.png" alt="linkedin logo" width="25"><a/> &nbsp;&nbsp;  <a href="https://mail.google.com/mail/?view=cm&fs=1&to=mckakade@outlook.com"><img src="https://logodix.com/logo/4406.png" alt="gmail logo" width="25"><a/> 
