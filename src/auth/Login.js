import React from "react";
import {
  Grid,
  TextField,
  Paper,
  makeStyles,
  Typography,
  InputAdornment,
  FormGroup,
  Button,
  FormHelperText,
  Icon
} from "@material-ui/core";
import { Link } from "react-router-dom";
import firebase from "./../firebase";
import { Mail, Lock } from "@material-ui/icons";
import CircularProgress from "@material-ui/core/CircularProgress";
import Snackbar from "@material-ui/core/Snackbar";
import MuiAlert from "@material-ui/lab/Alert";

const useStyles = makeStyles(theme => ({
  logo: {
    width: theme.spacing(40),
    padding: theme.spacing(1),
    height: theme.spacing(20)
  },
  formPaper: {
    width: theme.spacing(40),
    height: "auto",
    padding: theme.spacing(2),
    borderRadius: "6%"
  },
  formElements: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    marginTop: theme.spacing(1)
  },
  root: {
    backgroundColor: "#eee",
    height: "100vh",
    padding: theme.spacing(6),
    flexWrap: "nowrap"
  },
  overrideDisabled: {
    backgroundColor: "#bdbdbd !important"
  },
  title: {
    fontWeight: 700
  }
}));

const Login = props => {
  const styles = useStyles();

  const [state, setState] = React.useState({
    userName: "",
    email: "",
    password: "",
    isloading: false,
    firebaseErrors: ""
  });
  const [open, setOpen] = React.useState(false);

  const handleChange = event => {
    const { name, value } = event.target;
    setState(prevState => ({ ...prevState, [name]: value }));
  };

  const handleSubmit = event => {
    event.preventDefault();
    const { email, password } = state;

    if (email.length === 0 || password.length === 0) {
      return;
    } else {
      setState(prevState => ({
        ...prevState,
        isloading: true
      }));
      firebase
        .auth()
        .signInWithEmailAndPassword(state.email, state.password)
        .then(signedInUser => {
          setState(prevState => ({
            ...prevState,
            isloading: false,
            firebaseErrors: ""
          }));
          setOpen(false);
        })
        .catch(error => {
          console.log(error);
          setState(prevState => ({
            ...prevState,
            isloading: false,
            firebaseErrors: error.message
          }));
          setOpen(true);
        });
    }
  };

  const handleClose = (event, reason) => {
    setOpen(false);
  };

  return (
    <Grid
      className={styles.root}
      container
      alignItems="center"
      direction="column"
    >
      <Grid style={{ flexBasis: "30%" }} item xs={12}>
        <Typography>
          <Icon fontSize="small">
            <Paper
              style={{
                borderRadius: "50%",
                width: "53%",
                display: "flex",
                marginLeft: "24%",
                marginBottom: "5%",
                justifyContent: "center"
              }}
              component="span"
              elevation={7}
            >
              <img
                alt="message-logo"
                className={styles.logo}
                src="/Logos/google-messages.svg"
              />
            </Paper>
          </Icon>
        </Typography>
      </Grid>
      <Grid item xs={12}>
        <Paper className={styles.formPaper} elevation={7}>
          <form onSubmit={handleSubmit} noValidate autoComplete="disabled">
            <Typography className={styles.title} variant="h5" align="center">
              Login
            </Typography>
            <FormGroup>
              <TextField
                label="Email"
                name="email"
                variant="standard"
                className={styles.formElements}
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      <Mail color="secondary" />
                    </InputAdornment>
                  )
                }}
                autoComplete="new-password"
                onChange={handleChange}
              />
              <TextField
                label="Password"
                variant="standard"
                name="password"
                className={styles.formElements}
                type="password"
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      <Lock color="secondary" />
                    </InputAdornment>
                  )
                }}
                onChange={handleChange}
              />
              <Button
                className={styles.formElements}
                variant="contained"
                color="primary"
                type="submit"
                disabled={state.isloading}
                classes={{
                  disabled: styles.overrideDisabled
                }}
              >
                {state.isloading ? (
                  <CircularProgress size={20} color="secondary" />
                ) : (
                  <Typography variant="button">Submit</Typography>
                )}
              </Button>
            </FormGroup>
            <FormHelperText style={{ textAlign: "center" }} variant="standard">
              Don't have an account ? <Link to="/register">Register</Link>{" "}
            </FormHelperText>
          </form>
        </Paper>
      </Grid>
      <Snackbar
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "center"
        }}
        open={open}
        autoHideDuration={6000}
        onClose={handleClose}
      >
        <MuiAlert
          elevation={6}
          variant="filled"
          onClose={handleClose}
          severity="error"
        >
          {state.firebaseErrors}
        </MuiAlert>
      </Snackbar>
    </Grid>
  );
};

export default Login;
