import React from "react";
import {
  Grid,
  TextField,
  Paper,
  makeStyles,
  Typography,
  InputAdornment,
  FormGroup,
  Button,
  FormHelperText,
  Icon
} from "@material-ui/core";
import { Link } from "react-router-dom";
import firebase from "./../firebase";
import { AccountCircle, Mail, Lock } from "@material-ui/icons";
import CircularProgress from "@material-ui/core/CircularProgress";
import md5 from "md5";
import Snackbar from "@material-ui/core/Snackbar";
import MuiAlert from "@material-ui/lab/Alert";

const useStyles = makeStyles(theme => ({
  logo: {
    width: theme.spacing(40),
    padding: theme.spacing(1),
    height: theme.spacing(20)
  },
  formPaper: {
    width: theme.spacing(40),
    height: "auto",
    padding: theme.spacing(2),
    borderRadius: "6%"
  },
  formElements: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    marginTop: theme.spacing(1)
  },
  root: {
    backgroundColor: "#eee",
    height: "100vh",
    padding: theme.spacing(6),
    flexWrap: "nowrap"
  },
  overrideDisabled: {
    backgroundColor: "#bdbdbd !important"
  },
  title: {
    fontWeight: 700
  }
}));

const Register = props => {
  const styles = useStyles();

  const [state, setState] = React.useState({
    userName: "",
    email: "",
    password: "",
    confirmPassword: "",
    formErrors: {
      userName: "",
      email: "",
      password: "",
      confirmPassword: ""
    },
    isloading: false,
    usersRef: firebase.database().ref("users"),
    firebaseErrors: ""
  });
  const [open, setOpen] = React.useState(false);

  const validateUserName = value => {
    let formErrors = state.formErrors;

    if (value.search(/^([a-zA-Z]{3,8})$/g) === -1) {
      formErrors.userName =
        "Length must be atleast 3 letters and not exceed 8 letters";
    } else {
      formErrors.userName = "";
    }
    setState(prevState => ({
      ...prevState,
      formErrors: formErrors
    }));
  };

  const validateEmail = value => {
    let formErrors = state.formErrors;

    value.search(/\S+@\S+\.\S+$/) === -1
      ? (formErrors.email = "Invalid email format")
      : (formErrors.email = "");

    setState(prevState => ({
      ...prevState,
      formErrors: formErrors
    }));
  };

  const validatePassword = value => {
    let formErrors = state.formErrors;
    value.length < 6
      ? (formErrors.password = "Length must be atleast 6 letters")
      : (formErrors.password = "");
    setState(prevState => ({
      ...prevState,
      formErrors: formErrors
    }));
  };

  const validateConfirmPassword = value => {
    let { formErrors, password } = state;
    value !== password
      ? (formErrors.confirmPassword = "Passwords do not match")
      : (formErrors.confirmPassword = "");
    setState(prevState => ({
      ...prevState,
      formErrors: formErrors
    }));
  };

  const handleChange = event => {
    const { name, value } = event.target;
    setState(prevState => ({ ...prevState, [name]: value }));

    switch (name) {
      case "userName":
        validateUserName(value);
        break;
      case "email":
        validateEmail(value);
        break;
      case "password":
        validatePassword(value);
        break;
      case "confirmPassword":
        validateConfirmPassword(value);
        break;
      default:
        break;
    }

    // setState({ ...state, [event.target.name]: event.target.value });
  };

  const saveUserInDB = createdUser => {
    return state.usersRef.child(createdUser.user.uid).set({
      name: createdUser.user.displayName,
      avatar: createdUser.user.photoURL
    });
  };

  const handleSubmit = event => {
    event.preventDefault();
    const { formErrors, userName, email, password, confirmPassword } = state;

    if (
      formErrors.userName.length ||
      formErrors.email.length ||
      formErrors.password ||
      formErrors.confirmPassword.length ||
      userName.length === 0 ||
      email.length === 0 ||
      password.length === 0 ||
      confirmPassword.length === 0
    ) {
      return;
    } else {
      setState(prevState => ({
        ...prevState,
        isloading: true
      }));
      firebase
        .auth()
        .createUserWithEmailAndPassword(state.email, state.password)
        .then(createdUser => {
          createdUser.user
            .updateProfile({
              displayName: state.userName,
              photoURL: `http://gravatar.com/avatar/${md5(
                createdUser.user.email
              )}?d=identicon`
            })
            .then(() => {
              saveUserInDB(createdUser).then(() => {
                setState(prevState => ({
                  ...prevState,
                  isloading: false,
                  firebaseErrors: ""
                }));
                setOpen(false);
              });
            })
            .catch(error => {
              setState(prevState => ({
                ...prevState,
                isloading: false,
                firebaseErrors: error.message
              }));
              setOpen(true);
              console.error(error);
            });
        })
        .catch(error => {
          const formErrors = state.formErrors;
          error.message.toLowerCase().includes("email")
            ? (formErrors.email = error.message)
            : (formErrors.email = "");

          setState(prevState => ({
            ...prevState,
            isloading: false,
            formErrors: formErrors,
            firebaseErrors: error.message
          }));
          setOpen(true);
          console.error(error);
        });
    }
  };

  const handleClose = (event, reason) => {
    setOpen(false);
  };

  const formErrors = state.formErrors;

  return (
    <Grid
      className={styles.root}
      container
      alignItems="center"
      direction="column"
    >
      <Grid style={{ flexBasis: "30%" }} item xs={12}>
        <Typography>
          <Icon fontSize="small">
            <Paper
              style={{
                borderRadius: "50%",
                width: "53%",
                display: "flex",
                marginLeft: "24%",
                marginBottom: "5%",
                justifyContent: "center"
              }}
              component="span"
              elevation={7}
            >
              <img
                alt="message-logo"
                className={styles.logo}
                src="/Logos/google-messages.svg"
              />
            </Paper>
          </Icon>
        </Typography>
      </Grid>
      <Grid item xs={12}>
        <Paper className={styles.formPaper} elevation={7}>
          <form onSubmit={handleSubmit} noValidate autoComplete="disabled">
            <Typography className={styles.title} variant="h5" align="center">
              Register
            </Typography>
            <FormGroup>
              <TextField
                label="Username"
                name="userName"
                variant="standard"
                error={formErrors.userName.length > 0}
                helperText={formErrors.userName}
                className={styles.formElements}
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      <AccountCircle color="secondary" />
                    </InputAdornment>
                  )
                }}
                autoComplete="new-password"
                onChange={handleChange}
              />
              <TextField
                label="Email"
                name="email"
                variant="standard"
                helperText={formErrors.email}
                error={formErrors.email.length > 0}
                className={styles.formElements}
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      <Mail color="secondary" />
                    </InputAdornment>
                  )
                }}
                autoComplete="new-password"
                onChange={handleChange}
              />
              <TextField
                label="Password"
                variant="standard"
                name="password"
                error={formErrors.password.length > 0}
                helperText={formErrors.password}
                className={styles.formElements}
                type="password"
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      <Lock color="secondary" />
                    </InputAdornment>
                  )
                }}
                onChange={handleChange}
              />
              <TextField
                label="Confirm Password"
                variant="standard"
                name="confirmPassword"
                error={formErrors.confirmPassword.length > 0}
                helperText={formErrors.confirmPassword}
                className={styles.formElements}
                type="password"
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      <Lock color="secondary" />
                    </InputAdornment>
                  )
                }}
                onChange={handleChange}
              />
              <Button
                className={styles.formElements}
                variant="contained"
                color="primary"
                type="submit"
                disabled={state.isloading}
                classes={{
                  disabled: styles.overrideDisabled
                }}
              >
                {state.isloading ? (
                  <CircularProgress size={20} color="secondary" />
                ) : (
                  <Typography variant="button">Submit</Typography>
                )}
              </Button>
            </FormGroup>
            <FormHelperText style={{ textAlign: "center" }} variant="standard">
              Already a User ? <Link to="/login">Login</Link>{" "}
            </FormHelperText>
          </form>
        </Paper>
      </Grid>
      <Snackbar
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "center"
        }}
        open={open}
        autoHideDuration={6000}
        onClose={handleClose}
      >
        <MuiAlert
          elevation={6}
          variant="filled"
          onClose={handleClose}
          severity="error"
        >
          {state.firebaseErrors}
        </MuiAlert>
      </Snackbar>
    </Grid>
  );
};

export default Register;
