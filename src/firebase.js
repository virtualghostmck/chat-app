import firebase from "firebase/app";
import "firebase/auth";
import "firebase/database";
import "firebase/storage";

const firebaseConfig = {
  apiKey: "AIzaSyDThjY7WNTW0GbQYamGRHjPWtTrDyXU80o",
  authDomain: "chat-app-92889.firebaseapp.com",
  databaseURL: "https://chat-app-92889.firebaseio.com",
  projectId: "chat-app-92889",
  storageBucket: "chat-app-92889.appspot.com",
  messagingSenderId: "656004444905",
  appId: "1:656004444905:web:e6949a012f80ea71747678",
  measurementId: "G-0K4NDHN2JT"
};

firebase.initializeApp(firebaseConfig);

export default firebase;
