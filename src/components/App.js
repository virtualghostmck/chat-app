import React from "react";
import "./App.css";
import ColorPanel from "./ColorPanel/ColorPanel";
import SidePanel from "./SidePanel/SidePanel";
import MessageMetaContainer from "./Messages/MessageMetaContainer";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import { makeStyles } from "@material-ui/core";
import { CssBaseline } from "@material-ui/core";
import { connect } from "react-redux";

const useStyles = makeStyles(theme => ({
  container: {
    position: "relative",
    padding: 0,
    height: "100vh",
    backgroundColor: "#fafafa"
  },
  messageGrid: {
    flexGrow: 2,
    display: "flex"
  },
  metaGrid: {
    flexGrow: 1
  },
  sidePanelGrid: {
    flexGrow: 0,
    paddingLeft: "0px !important",
    width: theme.spacing(30)
  },
  colorPanelGrid: {
    flexBasis: "5%",
    height: "100%",
    backgroundColor: theme.palette.grey[900]
  },
  messageMetaGrid: {
    height: "80%"
  }
}));

const App = props => {
  const classes = useStyles();
  return (
    <React.Fragment>
      <CssBaseline />
      <Container className={classes.container} maxWidth="xl">
        <Grid
          style={{ margin: 0, width: "100%", height: "100%" }}
          container
          spacing={2}
        >
          {/* <Grid
            className={classes.colorPanelGrid}
            container
            direction="column"
            item
          >
            <ColorPanel />
          </Grid> */}
          <Grid className={classes.sidePanelGrid} item>
            <SidePanel currentUser={props.currentUser} />
          </Grid>
          <Grid style={{ flex: "1" }} item container direction="column">
            {props.currentChannel && (
              <MessageMetaContainer
                key={props.currentChannel.id}
                currentChannel={props.currentChannel}
                isPrivateChannel={props.isPrivateChannel}
              />
            )}
          </Grid>
        </Grid>
      </Container>
    </React.Fragment>
  );
};

const mapStateToProps = state => {
  return {
    currentUser: state.userReducer.currentUser,
    currentChannel: state.channelReducer.currentChannel,
    isPrivateChannel: state.channelReducer.isPrivateChannel
  };
};

export default connect(mapStateToProps)(App);
