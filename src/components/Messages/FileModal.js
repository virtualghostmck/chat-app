import React from "react";
import mime from "mime-types";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import { makeStyles, Typography, Snackbar } from "@material-ui/core";
import MuiAlert from "@material-ui/lab/Alert";
import ProgressBar from "../shared/ProgressBar";

const useStyles = makeStyles(theme => ({
  uploadBtn: {
    marginLeft: theme.spacing(1),
    color: theme.palette.grey[50]
  }
}));

export default function FileModal(props) {
  const { open, closeModal, uploadFile, uploadState, percentUploaded } = props;
  const classes = useStyles();

  const [file, setFile] = React.useState(null);
  const [showProgress, setShowProgress] = React.useState(false);
  const [openErrorBar, setOpenErrorBar] = React.useState(false);
  const authorizedTypes = ["image/jpeg", "image/png"];

  React.useEffect(() => {
    if (uploadState === "uploading") {
      setShowProgress(true);
    }
    if (uploadState === "done") {
      setShowProgress(false);
      setFile(null);
      closeModal();
    }
    if (uploadState === "error") {
      setOpenErrorBar(true);
      setFile(null);
    }
  }, [uploadState]);

  const addFile = e => {
    const file = e.target.files[0];
    setFile(file);
  };
  const sendFile = () => {
    if (file) {
      if (isAuthorized(file.name)) {
        const metaData = { contentType: mime.lookup(file.name) };
        uploadFile(file, metaData);
      }
    }
  };

  const handleClose = e => {
    setOpenErrorBar(false);
  };

  const isAuthorized = fileName =>
    authorizedTypes.includes(mime.lookup(fileName));

  return (
    <div>
      <Dialog
        open={open}
        fullWidth={true}
        onClose={e => {
          setFile(null);
          closeModal();
        }}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">Select an Image File</DialogTitle>
        <DialogContent>
          <Grid container alignItems="baseline">
            <DialogContentText>File Types : jpg, png</DialogContentText>
            <input
              accept="image/*"
              style={{ display: "none" }}
              id="raised-button-file"
              multiple
              type="file"
              onChange={addFile}
            />
            <label htmlFor="raised-button-file">
              <Button
                variant="contained"
                style={{ backgroundColor: "#F67B2B" }}
                size="small"
                className={classes.uploadBtn}
                component="span"
              >
                Choose File
              </Button>
            </label>
            {file && (
              <Typography style={{ paddingLeft: "8px" }} variant="caption">
                {file.name}
              </Typography>
            )}
          </Grid>
          {showProgress && <ProgressBar percentCompleted={percentUploaded} />}
          <Snackbar
            style={{ top: "45%" }}
            anchorOrigin={{
              vertical: "top",
              horizontal: "center"
            }}
            open={openErrorBar}
            autoHideDuration={4000}
            onClose={handleClose}
          >
            <MuiAlert
              elevation={6}
              variant="filled"
              onClose={handleClose}
              severity="error"
            >
              {`File size cannot be more than 1 MB`}
            </MuiAlert>
          </Snackbar>
        </DialogContent>
        <DialogActions>
          <Button
            variant="outlined"
            onClick={e => {
              setFile(null);
              closeModal();
            }}
            color="primary"
          >
            Cancel
          </Button>
          <Button onClick={sendFile} variant="outlined" color="primary">
            Send
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
