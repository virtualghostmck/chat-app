import React from "react";
import { connect } from "react-redux";
import { Grid, Paper, makeStyles } from "@material-ui/core";
import uuidv4 from "uuid/v4";
import MessageHeader from "./MessageHeader";
import MetaPanel from "../MetaPanel/MetaPanel";
import MessageForm from "./MessageFormClass";
import { Scrollbars } from "react-custom-scrollbars";
import firebase from "../../firebase";
import Message from "./Message";
import MessageSkeleton from "./MessageSkeleton";
import { capitalizeWord } from "../../utils/helpers";
import { setUserPosts } from "../../actions/actions";
import Typing from "./Typing";

const useStyles = makeStyles((theme) => ({
  commentGroup: {
    width: "100%",
    height: "100%",
    marginTop: theme.spacing(2),
    paddingLeft: theme.spacing(2),
    paddingTop: theme.spacing(2),
    paddingBottom: theme.spacing(2),
    paddingRight: theme.spacing(1),
  },
  messageMetaGrid: {
    height: "80%",
  },
  messageGrid: {
    flexGrow: 2,
    display: "flex",
    flexDirection: "column",
  },
  metaGrid: {
    flexGrow: 1,
    flexBasis: "10%",
  },
}));

const MessageMetaContainer = (props) => {
  const {
    currentChannel,
    currentUser,
    isPrivateChannel,
    setUserPosts,
    userPosts,
  } = props;
  const messageRefDOM = React.useRef("");
  const [messages, setMessages] = React.useState([]);
  const [messagesLoading, setMessagesLoading] = React.useState(true);
  const [isLoading, setIsLoading] = React.useState(false);
  const [errors, setErrors] = React.useState([]);
  const [uploadState, setUploadState] = React.useState("");
  const [uploadTask, setUploadTask] = React.useState(null);
  const [percentUploaded, setPercentUploaded] = React.useState(0);
  const [numUsers, setNumUsers] = React.useState("");
  const [searchRes, setSearchRes] = React.useState([]);
  const [isChannelStarred, setIsChannelStarred] = React.useState(false);
  const [typingUsers, setTypingUsers] = React.useState([]);
  const [listeners, setListeners] = React.useState([]);
  const listenersRef = React.useRef(listeners);
  const typingUsersRef = React.useRef(typingUsers);
  const scrollBars = React.useRef(null);
  const storageRef = firebase.storage().ref();
  const messageRef = firebase.database().ref("messages");
  const usersRef = firebase.database().ref("users");
  const privateMessagesRef = firebase.database().ref("privateMessages");
  const typingRef = firebase.database().ref("typing");
  const connectedRef = firebase.database().ref(".info/connected");
  const classes = useStyles();

  React.useEffect(() => {
    if (currentChannel && currentUser) {
      countTotalUserPosts([]);
      addListeners(currentChannel.id);
    }

    return removeRef;
  }, []);

  React.useEffect(() => {
    listenersRef.current = listeners;
  }, [listeners]);

  React.useEffect(() => {
    if (messages.length > 0) {
      scrollBars.current.scrollToBottom();
    }
  }, [messages.length, typingUsers.length]);

  const removeRef = () => {
    listenersRef.current.forEach((listener) => {
      listener.ref.child(listener.id).off(listener.event);
    });
    if (uploadTask !== null) {
      uploadTask.cancel();
    }
    connectedRef.off();
  };

  const addToListener = (id, ref, event) => {
    const index = listeners.findIndex(
      (listener) =>
        listener.id === id && listener.ref === ref && listener.event === event
    );

    if (index === -1) {
      const listener = {
        id: id,
        ref: ref,
        event: event,
      };
      setListeners([...listenersRef.current, listener]);
    }
  };

  const addListeners = (channelId) => {
    addMessageListener(channelId);
    addStarredChannelsListener(channelId, currentUser.uid);
    addTypingUsersListener(channelId);
  };

  const addStarredChannelsListener = (channelId, userId) => {
    usersRef
      .child(`${userId}/starred`)
      .once("value")
      .then((data) => {
        if (data.val() !== null) {
          const channelIds = Object.keys(data.val());
          const isPrevStarred = channelIds.includes(channelId);
          setIsChannelStarred(isPrevStarred);
        }
      });
  };

  React.useEffect(() => {
    typingUsersRef.current = [...typingUsers];
  }, [typingUsers]);

  const addTypingUsersListener = (channelId) => {
    typingRef.child(channelId).on("child_added", (snap) => {
      if (snap.key !== currentUser.uid) {
        const typingUser = {
          id: snap.key,
          name: snap.val(),
        };
        const currentTypingUsers = typingUsersRef.current;
        setTypingUsers([...currentTypingUsers, typingUser]);
      }
    });
    addToListener(channelId, typingRef, "child_added");

    typingRef.child(channelId).on("child_removed", (snap) => {
      const removedTypingUserId = snap.key;
      const currentTypingUsers = typingUsersRef.current;
      const removedTypingUserIndex = currentTypingUsers.findIndex(
        (user) => user.id === removedTypingUserId
      );
      if (removedTypingUserIndex !== -1) {
        const updatedTypingUsers = currentTypingUsers.filter(
          (user) => user.id !== removedTypingUserId
        );
        setTypingUsers([...updatedTypingUsers]);
      }
    });
    addToListener(channelId, typingRef, "child_removed");

    connectedRef.on("value", (snap) => {
      typingRef
        .child(channelId)
        .child(currentUser.uid)
        .onDisconnect()
        .remove((err) => {
          if (err !== null) {
            console.log(err);
          }
        });
    });
  };

  const addMessageListener = (channelId) => {
    let loadedMessages = [];
    getMessagesRef()
      .child(channelId)
      .on("child_added", (snap) => {
        loadedMessages.push(snap.val());
        setMessages([...loadedMessages]);
        countTotalUsers(loadedMessages);
        countTotalUserPosts(loadedMessages);
        setMessagesLoading(false);
      });
    addToListener(channelId, getMessagesRef(), "child_added");

    getMessagesRef()
      .child(channelId)
      .once("value", (snap) => {
        if (!snap.exists()) {
          setMessagesLoading(false);
        }
      });
  };

  const countTotalUserPosts = (messages) => {
    if (messages.length === 0) {
      setUserPosts(null);
    } else {
      const userPosts = messages.reduce((acc, message) => {
        if (message.createdBy.name in acc) {
          acc[message.createdBy.name].count += 1;
        } else {
          acc[message.createdBy.name] = {
            avatar: message.createdBy.avatar,
            count: 1,
          };
        }
        return acc;
      }, {});
      setUserPosts(userPosts);
    }
  };

  const handleChange = (e) => {
    const value = e.target.value;
    messageRefDOM.current.value = value;
    errors.length > 0 && setErrors([]);
  };

  const handleTyping = (e) => {
    if (messageRefDOM.current.value) {
      typingRef
        .child(currentChannel.id)
        .child(currentUser.uid)
        .set(currentUser.displayName);
    } else {
      typingRef.child(currentChannel.id).child(currentUser.uid).remove();
    }
  };

  const sendMessage = (e) => {
    if (messageRefDOM.current.value) {
      setIsLoading(true);
      getMessagesRef()
        .child(currentChannel.id)
        .push()
        .set(createNewMessage())
        .then((res) => {
          // setMessage("");
          messageRefDOM.current.value = "";
          typingRef.child(currentChannel.id).child(currentUser.uid).remove();
        })
        .catch((err) => {
          console.log(err);
          setErrors(errors.concat(err));
          setIsLoading(false);
        });
    } else {
      setErrors(errors.concat([{ message: "Message cannot be empty" }]));
    }
  };

  const createNewMessage = (fileURL = null) => {
    const newMessage = {
      timestamp: firebase.database.ServerValue.TIMESTAMP,
      createdBy: {
        id: currentUser.uid,
        name: currentUser.displayName,
        avatar: currentUser.photoURL,
      },
    };
    if (fileURL) {
      newMessage["image"] = fileURL;
    } else {
      newMessage["content"] = messageRefDOM.current.value;
    }
    return newMessage;
  };

  React.useEffect(() => {
    if (uploadState === "uploading") {
      uploadTask.on(
        "state_changed",
        (snap) => {
          let percentUploaded = Math.round(
            (snap.bytesTransferred / snap.totalBytes) * 100
          );
          setPercentUploaded(percentUploaded);
        },
        (err) => {
          console.log(err);
          setErrors(errors.concat(err));
          setUploadState("error");
          setPercentUploaded(0);
          setUploadTask(null);
        },
        () => {
          uploadTask.snapshot.ref
            .getDownloadURL()
            .then((downloadURL) => {
              sendFileMessage(downloadURL);
            })
            .catch((err) => {
              console.log(err);
              setErrors(errors.concat(err));
              setUploadState("error");
              setUploadTask(null);
            });
        }
      );
    }
  }, [uploadTask]);

  const getFilePath = () => {
    if (isPrivateChannel) {
      return `chat/private/${currentChannel.id}`;
    } else {
      return "chat/public";
    }
  };

  const uploadFile = (file, metaData) => {
    const filePathToUpload = `${getFilePath()}/${uuidv4()}.jpg`;
    setUploadState("uploading");
    setUploadTask(storageRef.child(filePathToUpload).put(file, metaData));
  };

  const sendFileMessage = (fileURL) => {
    if (fileURL) {
      setIsLoading(true);
      getMessagesRef()
        .child(currentChannel.id)
        .push()
        .set(createNewMessage(fileURL))
        .then((res) => {
          setUploadState("done");
        })
        .catch((err) => {
          console.log(err);
          setErrors(errors.concat(err));
          setIsLoading(false);
        });
    }
  };

  const getMessagesRef = () => {
    if (isPrivateChannel) {
      return privateMessagesRef;
    } else {
      return messageRef;
    }
  };

  const displayMessages = (messages) => {
    return (
      messages.length > 0 &&
      messages.map((message, index) => (
        <Message
          key={message.timestamp}
          message={message}
          currentUser={currentUser}
        />
      ))
    );
  };

  const displayChannelName = (channel) =>
    isPrivateChannel
      ? `${capitalizeWord(channel.name)}`
      : `${capitalizeWord(channel.name)}`;

  const countTotalUsers = (messages) => {
    const uniqueUsersList = messages.reduce((acc, curr) => {
      if (!acc.includes(curr.createdBy.name)) {
        acc.push(curr.createdBy.name);
      }
      return acc;
    }, []);

    const plural = uniqueUsersList.length !== 1;
    const numUsers = `${uniqueUsersList.length} user${plural ? "s" : ""}`;
    setNumUsers(numUsers);
  };

  const handleStar = () => {
    const updatedIsStarred = !isChannelStarred;
    setIsChannelStarred(updatedIsStarred);
    starChannels(updatedIsStarred);
  };

  const starChannels = (isChannelStarred) => {
    if (isChannelStarred) {
      usersRef.child(`${currentUser.uid}/starred`).update({
        [currentChannel.id]: {
          name: currentChannel.name,
          description: currentChannel.description,
          createdBy: {
            name: currentChannel.createdBy.name,
            avatar: currentChannel.createdBy.avatar,
          },
        },
      });
    } else {
      usersRef
        .child(`${currentUser.uid}/starred`)
        .child(currentChannel.id)
        .remove((err) => {
          if (err !== null) {
            console.log(err);
          }
        });
    }
  };

  const handleSearchChange = (e) => {
    const { value } = e.target;
    const regEx = new RegExp(value, "gi");
    const searchResults = messages.reduce((acc, message) => {
      if (
        (message.content && message.content.match(regEx)) ||
        message.createdBy.name.match(regEx)
      ) {
        acc.push(message);
      }
      return acc;
    }, []);
    value === "" ? setSearchRes([]) : setSearchRes([...searchResults]);
  };

  const displayTypingUsers = () => {
    if (typingUsers.length > 0) {
      return typingUsers.map((user) => {
        return (
          <Grid
            key={user.id}
            style={{
              display: "flex",
              alignItems: "center",
              marginBottom: "0.2em",
            }}
          >
            <span
              style={{
                fontStyle: "italic",
                fontWeight: "bold",
                marginRight: 3,
              }}
            >
              {" "}
              {user.name} is typing{" "}
            </span>{" "}
            <Typing />
          </Grid>
        );
      });
    }
    return null;
  };

  const displayMessageSkeleton = (loading) => {
    if (loading) {
      return [...Array(10)].map((value, index) => (
        <MessageSkeleton key={index} />
      ));
    }
  };

  return currentUser ? (
    <React.Fragment>
      <Grid
        className={classes.messageMetaGrid}
        item
        container
        spacing={2}
        direction="row"
      >
        <Grid className={classes.messageGrid} item>
          <MessageHeader
            channelName={displayChannelName(currentChannel)}
            totalUsers={numUsers}
            handleSearchChange={handleSearchChange}
            isPrivateChannel={isPrivateChannel}
            isChannelStarred={isChannelStarred}
            handleStar={handleStar}
            searchCount={searchRes.length}
            messagesLoading={messagesLoading}
          />
          <Paper elevation={5} className={classes.commentGroup}>
            <Scrollbars ref={scrollBars}>
              {displayMessageSkeleton(messagesLoading)}
              {searchRes.length > 0
                ? displayMessages(searchRes)
                : displayMessages(messages)}
              {displayTypingUsers()}
            </Scrollbars>
          </Paper>
        </Grid>
        {!isPrivateChannel && (
          <Grid className={classes.metaGrid} item>
            <MetaPanel
              key={currentChannel && currentChannel.id}
              isPrivateChannel={isPrivateChannel}
              currentChannel={currentChannel}
              userPosts={userPosts}
            />
          </Grid>
        )}
      </Grid>
      <Grid
        style={{ height: "20%", marginTop: "16px" }}
        direction="row"
        item
        container
      >
        <MessageForm
          handleChange={handleChange}
          sendMessage={sendMessage}
          uploadFile={uploadFile}
          errors={errors}
          isLoading={isLoading}
          message={messageRefDOM}
          uploadState={uploadState}
          percentUploaded={percentUploaded}
          isPrivateChannel={isPrivateChannel}
          handleTyping={handleTyping}
        />
      </Grid>
    </React.Fragment>
  ) : null;
};

const mapStateToProps = (state) => ({
  currentUser: state.userReducer.currentUser,
  userPosts: state.userReducer.userPosts,
});

const mapDispatchToProps = {
  setUserPosts,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MessageMetaContainer);
