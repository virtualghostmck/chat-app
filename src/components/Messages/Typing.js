import React from "react";
import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles(theme => ({
  typing: {
    width: "5em",
    height: "2em",
    position: "relative",
    padding: 10,
    marginLeft: 5,
    background: "#e6e6e6",
    borderRadius: 20
  },
  typingDot: {
    float: "left",
    width: 8,
    height: 8,
    marginLeft: 4,
    marginRight: 4,
    background: "#8d8c91",
    borderRadius: "50%",
    opacity: 0,
    animation: "$loadingFade 1s infinite",
    "&:nth-child(1)": {
      animationDelay: "0s"
    },
    "&:nth-child(2)": {
      animationDelay: "0.2s"
    },
    "&:nth-child(3)": {
      animationDelay: "0.4s"
    }
  },
  "@keyframes loadingFade": {
    "0%": {
      opacity: 0
    },
    "50%": {
      opacity: 0.8
    },
    "100%": {
      opacity: 0
    }
  }
}));

const Typing = () => {
  const styles = useStyles();
  return (
    <div className={styles.typing}>
      <div className={styles.typingDot}></div>
      <div className={styles.typingDot}></div>
      <div className={styles.typingDot}></div>
    </div>
  );
};

export default Typing;
