import React from "react";
import {
  Grid,
  Paper,
  TextField,
  Button,
  InputAdornment,
  Typography,
  IconButton,
  withStyles
} from "@material-ui/core";
import { Picker } from "emoji-mart";
import "emoji-mart/css/emoji-mart.css";
import AddBoxRoundedIcon from "@material-ui/icons/AddBoxRounded";
import ReplyRoundedIcon from "@material-ui/icons/ReplyRounded";
import CancelRoundedIcon from "@material-ui/icons/CancelRounded";
import CloudUploadRoundedIcon from "@material-ui/icons/CloudUploadRounded";
import FileModal from "./FileModal";

const styles = theme => ({
  formPaper: {
    display: "flex",
    width: "100%",
    height: "auto",
    padding: theme.spacing(2)
  },
  button: {
    flex: "1",
    borderRadius: "0%",
    color: theme.palette.grey[50]
  },
  input: {
    padding: theme.spacing(1)
  },
  buttonText: {
    flex: "1",
    textTransform: "none"
  },
  iconButton: {
    padding: theme.spacing(1),
    paddingLeft: theme.spacing(0),
    paddingRight: theme.spacing(0),
    color: theme.palette.grey[900]
  },
  emojipicker: {
    position: "absolute",
    zIndex: 1,
    bottom: 0,
    marginBottom: theme.spacing(18)
  }
});

class MessageForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      showEmojiPicker: false
    };
  }

  handleEmojiSelect = emojiObject => {
    const { message } = this.props;
    const unifiedArray = emojiObject.unified.split("-");
    const arrayUnicode = unifiedArray.map(item => {
      return `0x${item}`;
    });

    message.current.value = `${message.current.value} ${String.fromCodePoint(
      arrayUnicode
    )} `;

    message.current.focus();
  };

  handleTogglePicker = e => {
    this.setState({ showEmojiPicker: !this.state.showEmojiPicker });
  };

  closeModal = () => {
    this.setState({ open: false });
  };

  openModal = () => {
    this.setState({ open: true });
  };
  render() {
    const {
      handleChange,
      sendMessage,
      handleTyping,
      uploadFile,
      errors,
      message,
      uploadState,
      percentUploaded,
      classes
    } = this.props;
    const { open, showEmojiPicker } = this.state;
    return (
      <React.Fragment>
        {showEmojiPicker && (
          <div className={classes.emojipicker}>
            <Picker
              onSelect={this.handleEmojiSelect}
              set="google"
              title="Pick your emoji…"
              emoji="point_up"
            />
          </div>
        )}
        <Paper elevation={5} className={classes.formPaper}>
          <Grid item container direction="row" spacing={2}>
            <TextField
              error={errors.some(err => err.message)}
              inputRef={message}
              required
              onChange={handleChange}
              onKeyPress={e => {
                if (e.key === "Enter") {
                  if (showEmojiPicker) {
                    this.handleTogglePicker(e);
                  }
                  sendMessage(e);
                }
              }}
              onKeyDown={handleTyping}
              size="small"
              className={classes.input}
              variant="outlined"
              placeholder="Write your message"
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <IconButton
                      onClick={this.handleTogglePicker}
                      className={classes.iconButton}
                      aria-label="add"
                    >
                      {showEmojiPicker ? (
                        <CancelRoundedIcon />
                      ) : (
                        <AddBoxRoundedIcon />
                      )}
                    </IconButton>
                  </InputAdornment>
                )
              }}
              fullWidth
            />
            <Grid item container direction="row">
              <Button
                style={{ backgroundColor: "#F67B2B" }}
                variant="contained"
                className={classes.button}
                startIcon={<ReplyRoundedIcon htmlColor="#fff" />}
                onClick={e => {
                  if (showEmojiPicker) {
                    this.handleTogglePicker(e);
                  }
                  sendMessage(e);
                }}
              >
                <Typography className={classes.buttonText} variant="button">
                  Add Reply
                </Typography>
              </Button>
              <Button
                style={{ backgroundColor: "#49C39E" }}
                variant="contained"
                onClick={e => {
                  if (showEmojiPicker) {
                    this.handleTogglePicker(e);
                  }
                  this.openModal();
                }}
                className={classes.button}
                endIcon={<CloudUploadRoundedIcon htmlColor="#fff" />}
              >
                <Typography className={classes.buttonText} variant="button">
                  Upload Media
                </Typography>
              </Button>
              <FileModal
                open={open}
                closeModal={this.closeModal}
                uploadFile={uploadFile}
                uploadState={uploadState}
                percentUploaded={percentUploaded}
              />
            </Grid>
          </Grid>
        </Paper>
      </React.Fragment>
    );
  }
}

export default withStyles(styles)(MessageForm);
