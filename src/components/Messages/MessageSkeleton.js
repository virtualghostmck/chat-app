import React from "react";
import { Grid, makeStyles } from "@material-ui/core";
import Skeleton from "@material-ui/lab/Skeleton";

const useStyles = makeStyles(theme => ({
  avatar: {
    width: theme.spacing(5),
    height: theme.spacing(5)
  },
  content: {
    width: "90%",
    paddingLeft: theme.spacing(1),
    flexDirection: "column"
  },
  author: {
    width: theme.spacing(16)
  },
  message: {
    width: "100%"
  }
}));

const MessageSkeleton = () => {
  const classes = useStyles();

  const returnOwnMessage = () => {
    return (
      <Grid item className={classes.content}>
        <Skeleton animation="wave" className={classes.author} variant="text" />
        <Skeleton animation="wave" variant="text" className={classes.message} />
      </Grid>
    );
  };

  return (
    <React.Fragment>
      <Grid
        style={{ paddingBottom: "8px", flexWrap: "nowrap" }}
        alignItems="flex-start"
        justify="flex-start"
        container
        item
      >
        <Skeleton animation="wave" className={classes.avatar} variant="rect" />
        {returnOwnMessage()}
      </Grid>
    </React.Fragment>
  );
};

export default MessageSkeleton;
