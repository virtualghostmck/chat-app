import React from "react";
import {
  Grid,
  Paper,
  makeStyles,
  Typography,
  TextField,
  InputAdornment,
  IconButton,
} from "@material-ui/core";
import Skeleton from "@material-ui/lab/Skeleton";
import StarBorderRoundedIcon from "@material-ui/icons/StarBorderRounded";
import SearchRoundedIcon from "@material-ui/icons/SearchRounded";
import StarRoundedIcon from "@material-ui/icons/StarRounded";
import HighlightOffIcon from "@material-ui/icons/HighlightOff";

const useStyles = makeStyles((theme) => ({
  headerPaper: {
    width: "100%",
    height: "auto",
    display: "flex",
    padding: theme.spacing(2),
    justifyContent: "space-between",
  },
  title: {
    display: "flex",
    alignItems: "center",
  },
  helperText: {
    bottom: "95%",
    position: "absolute",
  },
}));

const MessageHeader = (props) => {
  const {
    channelName,
    totalUsers,
    handleSearchChange,
    isPrivateChannel,
    isChannelStarred,
    handleStar,
    searchCount,
    messagesLoading,
  } = props;
  const classes = useStyles();
  const searchRef = React.useRef(null);

  return (
    <Grid item container direction="row">
      <Paper elevation={5} className={classes.headerPaper}>
        <Grid item>
          <Typography className={classes.title} variant="h5">
            {channelName}{" "}
            {!isPrivateChannel ? (
              isChannelStarred ? (
                <StarRoundedIcon
                  style={{ cursor: "pointer", marginLeft: "4px" }}
                  onClick={handleStar}
                />
              ) : (
                <StarBorderRoundedIcon
                  style={{ cursor: "pointer", marginLeft: "4px" }}
                  onClick={handleStar}
                />
              )
            ) : null}
          </Typography>
          {!isPrivateChannel &&
            (!messagesLoading ? (
              <Typography variant="subtitle1">{totalUsers}</Typography>
            ) : (
              <Skeleton
                animation="wave"
                style={{ width: "64px", height: "28px" }}
                variant="text"
              />
            ))}
        </Grid>
        <Grid item>
          <TextField
            size="small"
            error={
              searchRef.current &&
              searchRef.current.value !== "" &&
              searchCount === 0
            }
            helperText={
              searchRef.current &&
              searchRef.current.value !== "" &&
              searchCount === 0 &&
              "Keyword Not Found"
            }
            FormHelperTextProps={{ classes: { root: classes.helperText } }}
            placeholder="Search"
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <SearchRoundedIcon />
                </InputAdornment>
              ),
              endAdornment: (
                <InputAdornment>
                  <IconButton
                    size="small"
                    aria-label="clear"
                    onClick={(e) => {
                      searchRef.current.value = "";
                      e.target.value = "";
                      handleSearchChange(e);
                    }}
                  >
                    <HighlightOffIcon />
                  </IconButton>
                </InputAdornment>
              ),
            }}
            inputProps={{
              maxLength: "65",
            }}
            inputRef={searchRef}
            variant="outlined"
            onChange={handleSearchChange}
          />
        </Grid>
      </Paper>
    </Grid>
  );
};

export default MessageHeader;
