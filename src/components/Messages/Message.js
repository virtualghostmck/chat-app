import React from "react";
import Image from "material-ui-image";
import { Grid, makeStyles, Typography, Avatar } from "@material-ui/core";
import { capitalizeWord } from "../../utils/helpers";
import moment from "moment";

const useStyles = makeStyles(theme => ({
  avatar: {
    width: theme.spacing(5),
    height: theme.spacing(5),
    border: "1px solid white"
  },
  content: {
    width: 600,
    paddingLeft: theme.spacing(1),
    marginLeft: theme.spacing(1),
    borderLeft: "2px solid #F67B2B",
    flexDirection: "column"
  },
  contentOther: {
    width: 600,
    paddingLeft: theme.spacing(1),
    marginLeft: theme.spacing(1),
    flexDirection: "column"
  },
  author: {
    fontWeight: "500"
  },
  message: {},
  timestamp: {
    marginLeft: theme.spacing(1),
    color: theme.palette.grey[400]
  }
}));

const Message = props => {
  const { message, currentUser } = props;
  const classes = useStyles();

  const timeFromNow = timestamp => moment(timestamp).fromNow();

  const isImage = message => {
    return (
      message.hasOwnProperty("image") && !message.hasOwnProperty("content")
    );
  };

  const returnOwnMessage = () => {
    if (message.createdBy.id === currentUser.uid) {
      return (
        <Grid item className={classes.content}>
          <Typography className={classes.author} component="span">
            <Typography variant="subtitle2" component="span">
              {capitalizeWord(message.createdBy.name)}
            </Typography>
            <Typography className={classes.timestamp} variant="caption">
              {timeFromNow(message.timestamp)}
            </Typography>
          </Typography>
          {isImage(message) ? (
            <Image animationDuration={1000} src={message.image} />
          ) : (
            <Typography
              className={classes.message}
              variant="body2"
              component="div"
            >
              {message.content}
            </Typography>
          )}
        </Grid>
      );
    } else {
      return (
        <Grid item className={classes.contentOther}>
          <Typography className={classes.author} component="span">
            <Typography variant="subtitle2" component="span">
              {capitalizeWord(message.createdBy.name)}
            </Typography>
            <Typography className={classes.timestamp} variant="caption">
              {timeFromNow(message.timestamp)}
            </Typography>
          </Typography>

          {isImage(message) ? (
            <Image animationDuration={1000} src={message.image} />
          ) : (
            <Typography
              className={classes.message}
              variant="body2"
              component="div"
            >
              {message.content}
            </Typography>
          )}
        </Grid>
      );
    }
  };

  return (
    <React.Fragment>
      <Grid
        style={{ paddingBottom: "8px" }}
        alignItems="flex-start"
        justify="flex-start"
        container
        item
      >
        <Avatar
          variant="rounded"
          className={classes.avatar}
          src={message.createdBy.avatar}
        />
        {returnOwnMessage()}
      </Grid>
    </React.Fragment>
  );
};
export default Message;
