import React from "react";
import { connect } from "react-redux";
import { Grid, Typography, makeStyles } from "@material-ui/core";
import ListAltRoundedIcon from "@material-ui/icons/ListAltRounded";
import IconButton from "@material-ui/core/IconButton";
import ToggleButton from "@material-ui/lab/ToggleButton";
import PlaylistAddIcon from "@material-ui/icons/PlaylistAdd";
import firebase from "../../firebase";
import AddChannel from "./AddChannel";
import { setCurrentChannel, setIsPrivateChannel } from "../../actions/actions";

const useStyles = makeStyles(theme => ({
  channelTitle: {
    flexGrow: 0.5
  },
  channelIcon: {
    flexGrow: 0.2
  },
  addChannel: {
    flexGrow: 0
  },
  channelGrid: {
    color: theme.palette.grey[500],
    padding: theme.spacing(2),
    paddingBottom: theme.spacing(0),
    paddingTop: theme.spacing(1)
  },
  channelButton: {
    justifyContent: "start",
    padding: theme.spacing(0),
    height: theme.spacing(3),
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(4),
    paddingLeft: theme.spacing(0.5),
    paddingRight: theme.spacing(1.5),
    border: "none"
  },
  buttonText: {
    color: theme.palette.grey[500],
    paddingBottom: theme.spacing(0.4),
    textTransform: "none",
    flex: 1,
    textAlign: "left"
  },
  notificationText: {
    color: theme.palette.grey[50],
    backgroundColor: "#C11623",
    flexGrow: 0.07,
    borderRadius: "10%"
  }
}));

const ChannelPanel = props => {
  const classes = useStyles();
  const {
    currentUser,
    setCurrentChannel,
    setIsPrivateChannel,
    currentChannel
  } = props;
  const [channels, setChannels] = React.useState([]);
  const channelsRef = React.useRef(channels);
  const [channel, setChannel] = React.useState({
    name: "",
    description: ""
  });
  const [firstChannel, setFirstChannel] = React.useState(null);
  const [activeChannel, setActiveChannel] = React.useState(null);
  const [notifications, setNotifications] = React.useState([]);
  const [open, setOpen] = React.useState(false);
  const myCurrentChannel = React.useRef(activeChannel);
  const channelRef = firebase.database().ref("channels");
  const messageRef = firebase.database().ref("messages");
  const typingRef = firebase.database().ref("typing");

  React.useEffect(() => {
    myCurrentChannel.current = activeChannel;
  }, [activeChannel]);

  React.useEffect(() => {
    channelsRef.current = channels;
  }, [channels]);

  React.useEffect(() => {
    return removeListener;
  }, []);

  const removeListener = () => {
    channelRef.off();
    channelsRef.current.forEach(channel => {
      messageRef.child(channel.id).off();
    });
  };

  React.useEffect(() => {
    addListener();
  }, []);

  React.useEffect(() => {
    if (channels.length > 0) {
      handleSelectChannel(firstChannel);
      setActiveChannel(firstChannel);
    }
  }, [firstChannel]);

  const addListener = () => {
    let loadedChannels = [];
    channelRef.on("child_added", snap => {
      loadedChannels.push(snap.val());
      setChannels([...loadedChannels]);
      setFirstChannel(loadedChannels[0]);
      addNotificationListener(snap.key);
    });
  };

  const addNotificationListener = channelID => {
    messageRef.child(channelID).on("value", snap => {
      const activeChannel = myCurrentChannel.current;
      if (activeChannel) {
        handleNotitifications(channelID, activeChannel.id, notifications, snap);
      }
    });
  };

  const handleNotitifications = (
    channelID,
    activeChannelID,
    notifications,
    snap
  ) => {
    let lastTotal = 0;
    let index = notifications.findIndex(
      notification => notification.id === channelID
    );
    if (index === -1) {
      const notification = {
        id: channelID,
        total: snap.numChildren(),
        lastKnownTotal: snap.numChildren(),
        count: 0
      };
      notifications.push(notification);
    } else {
      if (channelID !== activeChannelID) {
        lastTotal = notifications[index].total;

        if (snap.numChildren() - lastTotal > 0) {
          notifications[index].count = snap.numChildren() - lastTotal;
        }
      }
      notifications[index].lastKnownTotal = snap.numChildren();
    }

    setNotifications([...notifications]);
  };

  const handleClickOpen = () => {
    setOpen(true);
  };

  const clearNotifications = channel => {
    const index = notifications.findIndex(
      notification => notification.id === channel.id
    );

    if (index !== -1) {
      let updatedNotifications = [...notifications];
      updatedNotifications[index].total = notifications[index].lastKnownTotal;
      updatedNotifications[index].count = 0;
      setNotifications([...updatedNotifications]);
    }
  };

  const handleAddChannel = () => {
    const key = channelRef.push().key;
    const newChannel = {
      id: key,
      name: channel.name,
      description: channel.description,
      createdBy: {
        name: currentUser.displayName,
        avatar: currentUser.photoURL
      }
    };

    channelRef
      .child(key)
      .update(newChannel)
      .then(() => {
        setChannel({
          name: "",
          description: ""
        });
        setOpen(false);
      })
      .catch(error => {
        console.log(error);
      });
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleChange = event => {
    const { id, value } = event.target;
    if (id === "name") {
      setChannel(prevState => {
        return {
          ...prevState,
          name: value
        };
      });
    } else {
      setChannel(prevState => {
        return {
          ...prevState,
          description: value
        };
      });
    }
  };

  const handleSelectChannel = channel => {
    if (!currentChannel) {
      typingRef
        .child(channel.id)
        .child(currentUser.uid)
        .remove();
    } else {
      typingRef
        .child(currentChannel.id)
        .child(currentUser.uid)
        .remove();
    }
    setCurrentChannel(channel);
    setIsPrivateChannel(false);
    setActiveChannel(channel);
    clearNotifications(channel);
  };

  const getNotificationCount = channel => {
    let count = 0;
    const index = notifications.findIndex(
      notification => notification.id === channel.id
    );
    if (index !== -1) {
      count = notifications[index].count;
    }
    return count > 0 ? count : null;
  };

  const displayChannels = () => {
    return channels.map(channel => (
      <ToggleButton
        value={channel.name}
        key={channel.id}
        selected={currentChannel && channel.id === currentChannel.id}
        onChange={e => handleSelectChannel(channel)}
        className={classes.channelButton}
      >
        <Typography className={classes.buttonText} variant="caption">
          {"#"}
          {channel.name}
        </Typography>
        {getNotificationCount(channel) && (
          <Typography
            align="center"
            className={classes.notificationText}
            variant="caption"
          >
            {getNotificationCount(channel)}
          </Typography>
        )}
      </ToggleButton>
    ));
  };

  return (
    <Grid item container direction="column">
      <Grid
        // style={{ padding: "16px" }}
        item
        container
        justify="flex-start"
        alignItems="center"
        direction="row"
        className={classes.channelGrid}
      >
        <ListAltRoundedIcon className={classes.channelIcon} fontSize="small" />
        <Typography className={classes.channelTitle} variant="button">
          Channels ({channels.length})
        </Typography>
        <IconButton
          className={classes.addChannel}
          onClick={handleClickOpen}
          aria-label="delete"
        >
          <PlaylistAddIcon htmlColor="#9e9e9e" fontSize="small" />
        </IconButton>
        <AddChannel
          open={open}
          handleAddChannel={handleAddChannel}
          handleClose={handleClose}
          handleChange={handleChange}
          channel={channel}
        />
      </Grid>
      {displayChannels()}
    </Grid>
  );
};

const mapStateToProps = state => ({
  currentChannel: state.channelReducer.currentChannel
});

export default connect(mapStateToProps, {
  setCurrentChannel,
  setIsPrivateChannel
})(ChannelPanel);
