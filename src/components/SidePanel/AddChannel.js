import React from "react";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";

export default function AddChannel(props) {
  const { open, handleClose, handleAddChannel, handleChange, channel } = props;
  // const [channel, setChannel] = React.useState({
  //   name: "",
  //   description: ""
  // });
  // const handleChange = event => {
  //   const { id, value } = event.target;
  //   if (id === "name") {
  //     setChannel(prevState => {
  //       return {
  //         ...prevState,
  //         name: value
  //       };
  //     });
  //   } else {
  //     setChannel(prevState => {
  //       return {
  //         ...prevState,
  //         description: value
  //       };
  //     });
  //   }
  // };
  return (
    <div>
      <Dialog
        open={open}
        onClose={e => {
          handleClose(e);
        }}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">Add Channel</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Provide below details to add a channel.
          </DialogContentText>
          <TextField
            autoFocus
            value={channel.name}
            margin="dense"
            id="name"
            label="Name"
            type="text"
            fullWidth
            onChange={handleChange}
            required
          />
          <TextField
            value={channel.description}
            margin="dense"
            id="description"
            label="Description"
            type="text"
            fullWidth
            onChange={handleChange}
            required
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Cancel
          </Button>
          <Button
            onClick={event => {
              handleAddChannel();
            }}
            color="primary"
            disabled={
              channel.name.length === 0 || channel.description.length === 0
            }
          >
            Add
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
