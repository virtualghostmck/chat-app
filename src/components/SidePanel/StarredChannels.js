import React from "react";
import { connect } from "react-redux";
import firebase from "../../firebase";
import { Grid, Typography, makeStyles } from "@material-ui/core";
import StarRoundedIcon from "@material-ui/icons/StarRounded";
import ToggleButton from "@material-ui/lab/ToggleButton";
import { setCurrentChannel, setIsPrivateChannel } from "../../actions/actions";

const useStyles = makeStyles(theme => ({
  channelTitle: {
    flexGrow: 0.5
  },
  channelIcon: {
    flexGrow: 0.1
  },
  addChannel: {
    flexGrow: 0
  },
  channelGrid: {
    color: theme.palette.grey[500],
    padding: theme.spacing(2),
    paddingBottom: theme.spacing(0),
    paddingTop: theme.spacing(2)
  },
  channelButton: {
    justifyContent: "start",
    padding: theme.spacing(0),
    height: theme.spacing(3),
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(4),
    paddingLeft: theme.spacing(0.5),
    paddingRight: theme.spacing(1.5),
    border: "none"
  },
  buttonText: {
    color: theme.palette.grey[500],
    paddingBottom: theme.spacing(0.4),
    textTransform: "none",
    flex: 1,
    textAlign: "left"
  },
  notificationText: {
    color: theme.palette.grey[50],
    backgroundColor: "#C11623",
    flexGrow: 0.07,
    borderRadius: "10%"
  }
}));

export const StarredChannels = props => {
  const classes = useStyles();
  const {
    currentUser,
    setCurrentChannel,
    setIsPrivateChannel,
    currentChannel
  } = props;
  const [channels, setChannels] = React.useState([]);
  const channelsRef = React.useRef(channels);
  const usersRef = firebase.database().ref("users");
  React.useEffect(() => {
    addStarredChannelListener();
  }, []);

  React.useEffect(() => {
    channelsRef.current = channels;
  }, [channels]);

  const addStarredChannelListener = () => {
    usersRef.child(`${currentUser.uid}/starred`).on("child_added", snapshot => {
      const newStarredChannel = {
        id: snapshot.key,
        ...snapshot.val()
      };
      console.log(newStarredChannel);
      const prevStarred = [...channelsRef.current];
      console.log(prevStarred);
      setChannels([...prevStarred, newStarredChannel]);
    });

    usersRef
      .child(`${currentUser.uid}/starred`)
      .on("child_removed", snapshot => {
        const removedStarredChannel = {
          id: snapshot.key,
          ...snapshot.val()
        };
        const filterStarredChannels = channelsRef.current.filter(
          channel => channel.id !== removedStarredChannel.id
        );
        setChannels([...filterStarredChannels]);
      });
  };

  const displayStarredChannels = () => {
    return channels.map(channel => (
      <ToggleButton
        value={channel.name}
        key={channel.id}
        selected={currentChannel && channel.id === currentChannel.id}
        onChange={e => handleSelectChannel(channel)}
        className={classes.channelButton}
      >
        <Typography className={classes.buttonText} variant="caption">
          {"#"}
          {channel.name}
        </Typography>
      </ToggleButton>
    ));
  };

  const handleSelectChannel = channel => {
    setCurrentChannel(channel);
    setIsPrivateChannel(false);
  };

  return (
    <Grid item container direction="column">
      <Grid
        item
        container
        justify="flex-start"
        alignItems="center"
        direction="row"
        className={classes.channelGrid}
      >
        <StarRoundedIcon className={classes.channelIcon} fontSize="small" />
        <Typography className={classes.channelTitle} variant="button">
          Starred ({channels.length})
        </Typography>
      </Grid>
      {displayStarredChannels()}
    </Grid>
  );
};

const mapStateToProps = state => ({
  currentChannel: state.channelReducer.currentChannel
});

export default connect(mapStateToProps, {
  setCurrentChannel,
  setIsPrivateChannel
})(StarredChannels);
