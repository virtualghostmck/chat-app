import React from "react";
import firebase from "../../firebase";
import mime from "mime-types";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import { makeStyles, Typography, Avatar } from "@material-ui/core";
import SaveRoundedIcon from "@material-ui/icons/SaveRounded";
import VisibilityRoundedIcon from "@material-ui/icons/VisibilityRounded";
import CloseRoundedIcon from "@material-ui/icons/CloseRounded";
import AvatarEditor from "react-avatar-editor";

const useStyles = makeStyles(theme => ({
  uploadBtn: {
    marginLeft: theme.spacing(1),
    color: theme.palette.grey[50]
  },
  avatar: {
    width: theme.spacing(12),
    height: theme.spacing(12),
    border: "1px solid white",
    marginLeft: theme.spacing(12)
  }
}));

export default function AvatarModal(props) {
  const { open, closeModal } = props;
  const classes = useStyles();
  const [previewImage, setPreviewImage] = React.useState("");
  const [croppedImage, setCroppedImage] = React.useState("");
  const [uploadedImage, setUploadedImage] = React.useState("");
  const [blob, setBlob] = React.useState("");
  const [file, setFile] = React.useState(null);
  const avatarEditorRef = React.useRef(null);
  const storageRef = firebase.storage().ref();
  const userRef = firebase.auth().currentUser;
  const usersRef = firebase.database().ref("users");

  const addFile = e => {
    const file = e.target.files[0];
    setFile(file);
    if (file) {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.addEventListener("load", () => {
        setPreviewImage(reader.result);
      });
    }
  };

  const handlePreview = event => {
    const ref = avatarEditorRef.current;
    if (ref) {
      ref.getImageScaledToCanvas().toBlob(blob => {
        const imageURL = URL.createObjectURL(blob);
        setCroppedImage(imageURL);
        setBlob(blob);
      });
    }
  };

  React.useEffect(() => {
    if (uploadedImage) {
      userRef
        .updateProfile({
          photoURL: uploadedImage
        })
        .then(() => {})
        .catch(err => {
          console.log(err);
        });

      usersRef
        .child(userRef.uid)
        .update({
          avatar: uploadedImage
        })
        .then(() => {
          setCroppedImage("");
          setPreviewImage("");
          setUploadedImage("");
          setBlob("");
          setFile(null);
          closeModal();
          document.location.reload();
        })
        .catch(err => {
          console.log(err);
        });
    }
  }, [uploadedImage]);

  const uploadImage = () => {
    if (croppedImage) {
      const metaData = { contentType: mime.lookup(file.name) };
      storageRef
        .child(`avatars/users/${userRef.uid}`)
        .put(blob, metaData)
        .then(snapshot => {
          snapshot.ref.getDownloadURL().then(downloadURL => {
            setUploadedImage(downloadURL);
          });
        });
    }
  };

  return (
    <div>
      <Dialog
        open={open}
        fullWidth={true}
        onClose={e => {
          setCroppedImage("");
          setPreviewImage("");
          setUploadedImage("");
          setBlob("");
          setFile(null);
          closeModal();
        }}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">Select an Image File</DialogTitle>
        <DialogContent>
          <Grid container alignItems="baseline">
            <DialogContentText>File Types : jpg, png</DialogContentText>
            <input
              accept="image/*"
              style={{ display: "none" }}
              id="raised-button-file"
              multiple
              type="file"
              onChange={addFile}
            />
            <label htmlFor="raised-button-file">
              <Button
                variant="contained"
                style={{ backgroundColor: "#F67B2B" }}
                size="small"
                className={classes.uploadBtn}
                component="span"
              >
                Choose File
              </Button>
            </label>
            {file && (
              <Typography style={{ paddingLeft: "8px" }} variant="caption">
                {file.name}
              </Typography>
            )}
          </Grid>
          <Grid style={{ paddingTop: "8px" }} container>
            {previewImage && (
              <AvatarEditor
                ref={avatarEditorRef}
                image={previewImage}
                width={120}
                height={120}
                scale={1.2}
              />
            )}
            {croppedImage && (
              <Avatar
                variant="rounded"
                className={classes.avatar}
                src={croppedImage}
              />
            )}
          </Grid>
        </DialogContent>
        <DialogActions>
          <Button
            size="small"
            variant="outlined"
            onClick={e => {
              setCroppedImage("");
              setPreviewImage("");
              setUploadedImage("");
              setBlob("");
              setFile(null);
              closeModal();
            }}
            color="primary"
            startIcon={<CloseRoundedIcon />}
          >
            Cancel
          </Button>
          <Button
            size="small"
            onClick={handlePreview}
            variant="outlined"
            color="primary"
            startIcon={<VisibilityRoundedIcon />}
          >
            Preview
          </Button>
          <Button
            size="small"
            disabled={!croppedImage}
            onClick={uploadImage}
            variant="outlined"
            color="primary"
            startIcon={<SaveRoundedIcon />}
          >
            Change Avatar
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
