import React, { Component } from "react";
import { connect } from "react-redux";
import { Grid, Typography, withStyles } from "@material-ui/core";
import EmailRoundedIcon from "@material-ui/icons/EmailRounded";
import ToggleButton from "@material-ui/lab/ToggleButton";
import FiberManualRecordRoundedIcon from "@material-ui/icons/FiberManualRecordRounded";
import { setCurrentChannel, setIsPrivateChannel } from "../../actions/actions";
import firebase from "../../firebase";
import { capitalizeWord } from "../../utils/helpers";

const styles = theme => ({
  channelGrid: {
    color: theme.palette.grey[500],
    padding: theme.spacing(2),
    paddingBottom: theme.spacing(0),
    paddingTop: theme.spacing(1)
  },
  channelIcon: {
    flexGrow: 0.2
  },
  channelTitle: {
    flexGrow: 0.5
  },
  addChannel: {
    flexGrow: 0
  },
  channelButton: {
    justifyContent: "start",
    padding: theme.spacing(0),
    height: theme.spacing(3),
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(4),
    paddingLeft: theme.spacing(0.5),
    border: "none"
  },
  buttonText: {
    color: theme.palette.grey[500],
    paddingBottom: theme.spacing(0.4),
    textTransform: "none",
    flex: "1"
  }
});

class DirectMessages extends Component {
  constructor(props) {
    super(props);

    this.state = {
      usersRef: firebase.database().ref("users"),
      users: [],
      connectedRef: firebase.database().ref(".info/connected"),
      presenceRef: firebase.database().ref("presence")
    };
  }

  componentDidMount() {
    if (this.props.currentUser) {
      this.addListeners(this.props.currentUser.uid);
    }
  }

  componentWillUnmount() {
    this.state.usersRef.off();
    this.state.connectedRef.off();
    this.state.presenceRef.off();
  }

  addListeners = currentUserID => {
    let loadedUsers = [];
    this.state.usersRef.on("child_added", snap => {
      if (currentUserID !== snap.key) {
        let user = snap.val();
        user["status"] = "offline";
        user["uid"] = snap.key;
        loadedUsers.push(user);
        this.setState({ users: loadedUsers });
      }
    });

    this.state.connectedRef.on("value", snap => {
      if (snap.val() === true) {
        const ref = this.state.presenceRef.child(currentUserID);
        ref.set(true);
        ref.onDisconnect().remove(err => {
          if (err !== null) {
            console.log(err);
          }
        });
      }
    });

    this.state.presenceRef.on("child_added", snap => {
      if (snap.key !== currentUserID) {
        this.addStatusOfUser(snap.key);
      }
    });

    this.state.presenceRef.on("child_removed", snap => {
      if (snap.key !== currentUserID) {
        this.addStatusOfUser(snap.key, false);
      }
    });
  };

  addStatusOfUser = (userID, connected = true) => {
    const updatedUsers = this.state.users.reduce((acc, user) => {
      if (user.uid === userID) {
        user["status"] = connected ? "online" : "offline";
      }
      acc.push(user);
      return acc;
    }, []);

    this.setState({ users: updatedUsers });
  };

  handleSelectUser = user => {
    const channelID = this.getChannelID(user.uid);
    const channelData = {
      id: channelID,
      name: user.name
    };
    this.props.setCurrentChannel(channelData);
    this.props.setIsPrivateChannel(true);
  };

  getChannelID = userID => {
    const { currentUser } = this.props;
    return userID < currentUser.uid
      ? `${userID}/${currentUser.uid}`
      : `${currentUser.uid}/${userID}`;
  };

  displayUsers = (users, classes) => {
    return users.map(user => {
      return (
        <ToggleButton
          value={user.name}
          key={user.uid}
          selected={
            this.props.currentChannel &&
            this.getChannelID(user.uid) === this.props.currentChannel.id
          }
          onChange={e => this.handleSelectUser(user)}
          className={classes.channelButton}
        >
          <Typography
            align="left"
            className={classes.buttonText}
            variant="caption"
          >
            {"@"}
            {capitalizeWord(user.name.toLowerCase())}
          </Typography>
          <FiberManualRecordRoundedIcon
            className={classes.channelIcon}
            fontSize="small"
            htmlColor={this.isUserOnline(user) ? "#35ac19" : "#D40E0D"}
          />
        </ToggleButton>
      );
    });
  };

  isUserOnline = user => {
    return user.status === "online" ? true : false;
  };

  render() {
    const { classes } = this.props;
    return (
      <Grid item container direction="column">
        <Grid
          // style={{ padding: "16px" }}
          item
          container
          justify="flex-start"
          alignItems="center"
          direction="row"
          className={classes.channelGrid}
        >
          <EmailRoundedIcon className={classes.channelIcon} fontSize="small" />
          <Typography className={classes.channelTitle} variant="button">
            Direct Messages ({this.state.users.length})
          </Typography>
        </Grid>
        {this.displayUsers(this.state.users, classes)}
      </Grid>
    );
  }
}

const mapsDispatchToProps = {
  setCurrentChannel,
  setIsPrivateChannel
};

const mapStateToProps = state => ({
  currentChannel: state.channelReducer.currentChannel
});

const connectedDirectMessages = connect(
  mapStateToProps,
  mapsDispatchToProps
)(DirectMessages);

export default withStyles(styles)(connectedDirectMessages);
