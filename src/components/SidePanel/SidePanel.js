import React, { Component } from "react";
import {
  Drawer,
  Divider,
  Typography,
  Grid,
  Select,
  MenuItem,
  FormControl,
  InputAdornment,
  Avatar
} from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import DeveloperModeIcon from "@material-ui/icons/DeveloperMode";
import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";
import firebase from "../../firebase";
import ChannelPanel from "./ChannelPanel";
import { capitalizeWord } from "../../utils/helpers";
import DirectMessages from "./DirectMessages";
import StarredChannels from "./StarredChannels";
import AvatarModal from "./AvatarModal";
// import {} from "@material-ui/core/colors/"

const drawerWidth = 240;

const styles = theme => ({
  drawer: { width: drawerWidth, flexShrink: 0 },
  drawerPaper: { width: drawerWidth, backgroundColor: "#3F0F3F" },
  toolbar: {
    display: "flex",
    alignItems: "center",
    minHeight: "56px",
    padding: theme.spacing(2)
  },
  overridePaperAnchorLeft: {
    left: "auto"
  },
  title: {
    fontWeight: 700,
    color: "#fff"
  },
  userPanel: {
    width: "auto",
    marginTop: theme.spacing(2),
    marginLeft: theme.spacing(2.75)
  },
  overRideIcon: {
    color: "#fff"
  },
  avatar: {
    width: theme.spacing(3),
    height: theme.spacing(3),
    border: "1px solid"
  }
});

class SidePanel extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: props.currentUser,
      isOpenModal: false
    };
  }

  closeModal = () => {
    this.setState({ isOpenModal: false });
  };

  handleChange = event => {
    const { value } = event.target;
    switch (value) {
      case "sign-out":
        firebase
          .auth()
          .signOut()
          .then(() => {});
        break;
      case "change-avatar":
        this.setState({ isOpenModal: true });
        break;
      default:
        break;
    }
  };

  render() {
    const { classes } = this.props;
    const { user } = this.state;
    return (
      user && (
        <Drawer
          className={(classes.drawer, classes.toolBarMain)}
          variant="permanent"
          classes={{
            paper: classes.drawerPaper,
            paperAnchorLeft: classes.overridePaperAnchorLeft
          }}
          anchor="left"
        >
          <div className={classes.toolbar}>
            <Grid justify="flex-start" alignItems="center" container>
              <DeveloperModeIcon fontSize="large" htmlColor="#fff" />
              <Typography className={classes.title} variant="h5">
                Chat-Console
              </Typography>
            </Grid>
          </div>
          <Divider />
          <Grid justify="flex-start" container>
            <FormControl className={classes.userPanel}>
              <Select
                labelId="user-panel"
                disableUnderline
                id="user-panel-id"
                displayEmpty
                classes={{
                  icon: classes.overRideIcon
                }}
                style={{ color: "#fff" }}
                renderValue={value => capitalizeWord(user.displayName) + "  "}
                IconComponent={props => {
                  return <ArrowDropDownIcon {...props} />;
                }}
                startAdornment={
                  <InputAdornment position="start">
                    <Avatar className={classes.avatar} src={user.photoURL} />
                  </InputAdornment>
                }
                MenuProps={{
                  getContentAnchorEl: null,
                  anchorOrigin: {
                    vertical: "bottom",
                    horizontal: "left"
                  }
                }}
                onChange={this.handleChange}
                value=""
              >
                <MenuItem disabled>Signed in as {user.displayName}</MenuItem>
                <MenuItem value={"change-avatar"}>Change Avatar</MenuItem>
                <MenuItem value={"sign-out"}>Sign Out</MenuItem>
              </Select>
            </FormControl>
            <AvatarModal
              open={this.state.isOpenModal}
              closeModal={this.closeModal}
            />
            <Grid item container direction="column">
              <StarredChannels currentUser={user} />
            </Grid>
            <Grid item container direction="column">
              <ChannelPanel currentUser={user} />
            </Grid>
            <Grid item container direction="column">
              <DirectMessages currentUser={user} />
            </Grid>
          </Grid>
        </Drawer>
      )
    );
  }
}

export default withStyles(styles)(SidePanel);
