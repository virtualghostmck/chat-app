import React from "react";
import LinearProgress from "@material-ui/core/LinearProgress";
import { lighten, makeStyles, withStyles } from "@material-ui/core/styles";

const BorderLinearProgress = withStyles({
  root: {
    height: 10,
    backgroundColor: lighten("#ff6c5c", 0.5)
  },
  bar: {
    borderRadius: 20,
    backgroundColor: "#ff6c5c"
  }
})(LinearProgress);

const useStyles = makeStyles(theme => ({
  margin: {
    marginTop: theme.spacing(1)
  }
}));

const ProgressBar = props => {
  const { percentCompleted } = props;
  const classes = useStyles();
  return (
    <div>
      <BorderLinearProgress
        className={classes.margin}
        variant="determinate"
        color="secondary"
        value={percentCompleted}
      />
    </div>
  );
};

export default ProgressBar;
