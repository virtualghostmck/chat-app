import React from "react";
import Backdrop from "@material-ui/core/Backdrop";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core";
import CircularProgress from "@material-ui/core/CircularProgress";

const useStyles = makeStyles(theme => ({
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: "#fff",
    flexDirection: "column"
  },
  messsage: {
    paddingTop: "2%"
  }
}));

const Spinner = props => {
  const classes = useStyles();
  return (
    <Backdrop className={classes.backdrop} open={true}>
      <CircularProgress color="inherit" />
      <Typography className={classes.messsage} variant="h6">
        Preparing Chat ...
      </Typography>
    </Backdrop>
  );
};

export default Spinner;
