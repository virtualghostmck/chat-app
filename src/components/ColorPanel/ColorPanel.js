import React, { Component } from "react";
import { Divider, IconButton, withStyles } from "@material-ui/core";
import AddBoxRoundedIcon from "@material-ui/icons/AddBoxRounded";

const styles = theme => ({
  iconButton: {
    padding: theme.spacing(1)
  }
});

class ColorPanel extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    const { classes } = this.props;
    return (
      <React.Fragment>
        <Divider style={{ backgroundColor: "#757575" }} />
        <IconButton
          className={classes.iconButton}
          size="medium"
          color="primary"
        >
          <AddBoxRoundedIcon fontSize="large" />
        </IconButton>
      </React.Fragment>
    );
  }
}

export default withStyles(styles)(ColorPanel);
