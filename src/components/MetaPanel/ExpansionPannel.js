import React from "react";
import { withStyles, makeStyles } from "@material-ui/core/styles";
import { Grid, Avatar } from "@material-ui/core";
import MuiExpansionPanel from "@material-ui/core/ExpansionPanel";
import MuiExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import MuiExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import Typography from "@material-ui/core/Typography";
import InfoRoundedIcon from "@material-ui/icons/InfoRounded";
import AccountCircleRoundedIcon from "@material-ui/icons/AccountCircleRounded";
import EditRoundedIcon from "@material-ui/icons/EditRounded";
import { capitalizeWord } from "../../utils/helpers";
const ExpansionPanel = withStyles({
  root: {
    border: "1px solid rgba(0, 0, 0, .125)",
    boxShadow: "none",
    "&:not(:last-child)": {
      borderBottom: 0
    },
    "&:before": {
      display: "none"
    },
    "&$expanded": {
      margin: "auto"
    }
  },
  expanded: {}
})(MuiExpansionPanel);

const ExpansionPanelSummary = withStyles({
  root: {
    backgroundColor: "rgba(0, 0, 0, .03)",
    borderBottom: "1px solid rgba(0, 0, 0, .125)",
    marginBottom: -1,
    minHeight: 56,
    "&$expanded": {
      minHeight: 56
    }
  },
  content: {
    "&$expanded": {
      margin: "12px 0"
    }
  },
  expanded: {}
})(MuiExpansionPanelSummary);

const ExpansionPanelDetails = withStyles(theme => ({
  root: {
    padding: theme.spacing(2)
  }
}))(MuiExpansionPanelDetails);

const useStyles = makeStyles(theme => ({
  exPanelTitle: {
    paddingLeft: theme.spacing(1),
    fontSize: "medium"
  },
  avatar: {
    width: theme.spacing(5),
    height: theme.spacing(5),
    border: "1px solid white"
  },
  posterName: {
    paddingLeft: theme.spacing(1),
    fontWeight: "500"
  },
  timestamp: {
    marginLeft: theme.spacing(1),
    color: theme.palette.grey[500]
  },
  posterGrid: {
    paddingLeft: theme.spacing(1),
    marginLeft: theme.spacing(1),
    flexDirection: "column"
  }
}));

export default function CustomizedExpansionPanels(props) {
  const [expanded, setExpanded] = React.useState("panel1");
  const classes = useStyles();
  const handleChange = panel => (event, newExpanded) => {
    setExpanded(newExpanded ? panel : false);
  };
  const { currentChannel, userPosts } = props;

  const displayPostCount = count => {
    return count > 1 ? `${count} posts` : `${count} post`;
  };

  const displayTopPosters = userPosts => {
    const sortedUserPosts = Object.entries(userPosts).sort(
      (a, b) => b[1].count - a[1].count
    );
    return sortedUserPosts.map((userPost, index) => {
      return (
        <Grid
          key={index}
          style={{ paddingBottom: "8px" }}
          alignItems="center"
          justify="flex-start"
          container
          item
        >
          <Avatar
            variant="circle"
            className={classes.avatar}
            src={userPost[1].avatar}
          />
          <Grid item className={classes.posterGrid}>
            <Typography
              component="span"
              variant="subtitle2"
              className={classes.posterName}
            >
              {`${capitalizeWord(userPost[0])}`}
            </Typography>
            <Typography
              component="div"
              className={classes.timestamp}
              variant="caption"
            >
              {displayPostCount(userPost[1].count)}
            </Typography>
          </Grid>
        </Grid>
      );
    });
  };

  return (
    <React.Fragment>
      <ExpansionPanel
        square
        expanded={expanded === "panel1"}
        onChange={handleChange("panel1")}
      >
        <ExpansionPanelSummary
          aria-controls="panel1d-content"
          id="panel1d-header"
        >
          <InfoRoundedIcon />{" "}
          <Typography variant="subtitle2" className={classes.exPanelTitle}>
            Channel Details
          </Typography>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails>
          <Typography style={{ fontSize: "0.9rem" }} variant="caption">
            {currentChannel.description}
          </Typography>
        </ExpansionPanelDetails>
      </ExpansionPanel>
      <ExpansionPanel
        square
        expanded={expanded === "panel2"}
        onChange={handleChange("panel2")}
      >
        <ExpansionPanelSummary
          aria-controls="panel2d-content"
          id="panel2d-header"
        >
          <AccountCircleRoundedIcon />{" "}
          <Typography variant="subtitle2" className={classes.exPanelTitle}>
            Top Posters
          </Typography>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails style={{ flexDirection: "column" }}>
          <Typography></Typography>
          {userPosts && displayTopPosters(userPosts)}
        </ExpansionPanelDetails>
      </ExpansionPanel>
      <ExpansionPanel
        square
        expanded={expanded === "panel3"}
        onChange={handleChange("panel3")}
      >
        <ExpansionPanelSummary
          aria-controls="panel3d-content"
          id="panel3d-header"
        >
          <EditRoundedIcon />{" "}
          <Typography variant="subtitle2" className={classes.exPanelTitle}>
            Created By
          </Typography>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails>
          <Grid
            style={{ paddingBottom: "8px" }}
            alignItems="center"
            justify="flex-start"
            container
            item
          >
            <Avatar
              variant="circle"
              className={classes.avatar}
              src={currentChannel.createdBy.avatar}
            />
            <Typography variant="subtitle2" style={{ paddingLeft: "8px" }}>
              {capitalizeWord(currentChannel.createdBy.name)}
            </Typography>
          </Grid>
        </ExpansionPanelDetails>
      </ExpansionPanel>
    </React.Fragment>
  );
}
