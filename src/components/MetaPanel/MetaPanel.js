import React, { Component } from "react";
import {
  Grid,
  Typography,
  Paper,
  withStyles,
  Card,
  CardContent
} from "@material-ui/core";
import ExpansionPannel from "./ExpansionPannel";

const styles = theme => ({});

class MetaPanel extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { classes, isPrivateChannel, currentChannel, userPosts } = this.props;
    if (isPrivateChannel || !currentChannel) return null;
    return (
      <Grid item container direction="column">
        <Paper style={{ padding: 0 }} elevation={5}>
          <Card className={classes.root}>
            <CardContent>
              <Typography align="center" variant="h5" component="h2">
                About #{currentChannel.name}
              </Typography>
              <Grid item style={{ paddingTop: "8px" }}>
                <ExpansionPannel
                  currentChannel={currentChannel}
                  userPosts={userPosts}
                />
              </Grid>
            </CardContent>
          </Card>
        </Paper>
      </Grid>
    );
  }
}

export default withStyles(styles)(MetaPanel);
