export const capitalizeWord = word => {
  if (typeof word === "string") {
    return word[0].toUpperCase() + word.slice(1);
  } else {
    return "";
  }
};
