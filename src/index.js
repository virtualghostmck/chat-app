import React, { Component } from "react";
import ReactDOM from "react-dom";
import { createStore } from "redux";
import { Provider, connect } from "react-redux";
import rootReducer from "./reducers/index";
import { setUser, clearUser } from "./actions/actions";
import { composeWithDevTools } from "redux-devtools-extension";
import "./index.css";
import App from "./components/App";
import Login from "./auth/Login";
import Register from "./auth/Register";
import firebase from "./firebase";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  withRouter
} from "react-router-dom";
import Spinner from "./components/shared/Spinner";
import registerServiceWorker from "./registerServiceWorker";

const store = createStore(rootReducer, composeWithDevTools());

//Funtion method to call mapDispatchToProps
// const mapDispatchToProps = dispatch => {
//   return {
//     setUser: user => {
//       dispatch(setUser(user));
//     }
//   };
// };

const mapDispatchToProps = {
  setUser,
  clearUser
};

const mapStateToProps = state => {
  return {
    isLoading: state.userReducer.isLoading
  };
};

class Root extends Component {
  componentDidMount() {
    firebase.auth().onAuthStateChanged(user => {
      if (user) {
        this.props.setUser(user);
        this.props.history.push("/");
      } else {
        this.props.clearUser();
        this.props.history.push("/login");
      }
    });
  }

  render() {
    return this.props.isLoading ? (
      <Spinner />
    ) : (
      <Switch>
        <Route exact path="/" component={App} />
        <Route path="/login" component={Login} />
        <Route path="/register" component={Register} />
      </Switch>
    );
  }
}

const RootWithRoute = withRouter(
  connect(mapStateToProps, mapDispatchToProps)(Root)
);

ReactDOM.render(
  <Provider store={store}>
    <Router>
      <RootWithRoute />
    </Router>
  </Provider>,
  document.getElementById("root")
);
registerServiceWorker();
