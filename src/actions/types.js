import KeyMirror from "keymirror";

export const types = KeyMirror({
  //User action types
  SET_USER: null,
  CLEAR_USER: null,
  SET_USER_POSTS: null,
  //Channel action types
  SET_CURRENT_CHANNEL: null,
  SET_ISPRIVATE_CHANNEL: null
});
