import { types } from "./types";

//User Actions
export const setUser = user => ({
  type: types.SET_USER,
  payload: {
    currentUser: user
  }
});

export const clearUser = () => {
  return { type: types.CLEAR_USER };
};

export const setUserPosts = userPosts => {
  return {
    type: types.SET_USER_POSTS,
    payload: {
      userPosts
    }
  };
};

//Channel Actions
export const setCurrentChannel = channel => {
  return {
    type: types.SET_CURRENT_CHANNEL,
    payload: {
      currentChannel: channel
    }
  };
};

export const setIsPrivateChannel = isPrivateChannel => {
  return {
    type: types.SET_ISPRIVATE_CHANNEL,
    payload: {
      isPrivateChannel: isPrivateChannel
    }
  };
};
