import { combineReducers } from "redux";
import { types } from "../actions/types";
const initUserState = {
  currentUser: null,
  isLoading: true,
  userPosts: null
};
//User Reducer
const UserReducer = (state = initUserState, action) => {
  switch (action.type) {
    case types.SET_USER:
      return {
        ...state,
        currentUser: action.payload.currentUser,
        isLoading: false
      };
    case types.CLEAR_USER:
      return {
        ...state,
        currentUser: null,
        isLoading: false
      };
    case types.SET_USER_POSTS:
      return {
        ...state,
        userPosts: action.payload.userPosts
      };
    default:
      return state;
  }
};

//Channel reducer
const initChannelState = {
  currentChannel: null,
  isPrivateChannel: false
};

const channelReducer = (state = initChannelState, action) => {
  switch (action.type) {
    case types.SET_CURRENT_CHANNEL:
      return {
        ...state,
        currentChannel: action.payload.currentChannel
      };
    case types.SET_ISPRIVATE_CHANNEL:
      return {
        ...state,
        isPrivateChannel: action.payload.isPrivateChannel
      };
    default:
      return state;
  }
};

const rootReducer = combineReducers({
  userReducer: UserReducer,
  channelReducer: channelReducer
});

export default rootReducer;
